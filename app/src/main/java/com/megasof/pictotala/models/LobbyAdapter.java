package com.megasof.pictotala.models;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.megasof.pictotala.Globals;
import com.megasof.pictotala.fragments.PasswdLobbyDialog;
import com.megasof.pictotala.R;

import java.util.List;

public class LobbyAdapter extends RecyclerView.Adapter<LobbyAdapter.MyViewHolder> {

    private Context mContext;
    private List<Lobby> mData;

    public LobbyAdapter(Context mContext, List<Lobby> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.lobby_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int i) {
        holder.name.setText(mData.get(i).getName());
        String textCreator = mContext.getString(R.string.cardLobby_creator)+mData.get(i).getCreator();
        holder.creator.setText(textCreator);
        holder.numbOfUser.setText(mData.get(i).getCurrentUserOnMax());
        holder.id.setText(mData.get(i).getId());
        holder.padlock.setVisibility(mData.get(i).isHasPasswd() ? View.VISIBLE : View.INVISIBLE );
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //implement onClick

                if (holder.padlock.getVisibility() == View.VISIBLE) {
                    DialogFragment newFragment = new PasswdLobbyDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("id",holder.id.getText().toString());
                    bundle.putString("name",holder.name.getText().toString());
                    newFragment.setArguments(bundle);
                    newFragment.show(((FragmentActivity)mContext).getSupportFragmentManager(), "PasswdLobby");
                } else {
                    Globals.enterLobby(mContext, holder.id.getText().toString());
                }
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        TextView name, creator, numbOfUser, id;
        ImageView padlock;

        MyViewHolder(View itemView) {
            super(itemView);
            this.card = itemView.findViewById(R.id.cv_lobby);
            this.name = itemView.findViewById(R.id.tv_lobbyName);
            this.creator = itemView.findViewById(R.id.tv_lobbyCreator);
            this.numbOfUser = itemView.findViewById(R.id.tv_number);
            this.id = itemView.findViewById(R.id.tv_id);
            this.padlock = itemView.findViewById(R.id.iv_padlock);
        }
    }
}
