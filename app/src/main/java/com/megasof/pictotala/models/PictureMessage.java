package com.megasof.pictotala.models;

import java.util.Date;

public final class PictureMessage extends Message {
    private String picturePath;


    public PictureMessage(String picturePath) {
        this.picturePath = picturePath;
    }

    public PictureMessage(String authorId, String picturePath) {
        super(authorId);
        this.picturePath = picturePath;
    }

    public PictureMessage(String authorId, String messageId, Date dateCreated, String picturePath) {
        super(authorId, messageId, dateCreated);
        this.picturePath = picturePath;
    }

    // Getter
    public String getPicturePath() {
        return picturePath;
    }


    // Setter
    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }
}
