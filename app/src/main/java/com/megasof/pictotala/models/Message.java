package com.megasof.pictotala.models;

import java.util.Date;

public abstract class Message {
    private String authorId;
    private String authorName;
    private String messageId;
    private Date dateCreated;


    public Message() {}

    public Message(String authorId) {
        this.authorId = authorId;
    }

    public Message(String authorId, String messageId, Date dateCreated) {
        this.authorId = authorId;
        this.messageId = messageId;
        this.dateCreated = dateCreated;
    }

    // Setter
    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    // Getter
    public String getAuthorId() {
        return authorId;
    }

    public String getMessageId() {
        return messageId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getAuthorName() {
        return authorName;
    }
}
