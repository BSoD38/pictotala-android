package com.megasof.pictotala.models;

import com.google.firebase.firestore.DocumentReference;

public class Lobby {
    private String id;
    private String name;
    private String passwd;
    private boolean hasPasswd;
    private boolean isAlive;
    private boolean isKnown;
    private String creator;
    private DocumentReference uid;
    private int userCount;
    private int maxUsers;
    private boolean isWorldWide;

    public Lobby() {
        this.name = "";
        this.maxUsers = 0;
        this.userCount = 0;
        this.creator = "";
    }

    public Lobby(String id, String name, String passwd, String creator, int userCount, int maxUser, boolean hasPasswd, boolean isAlive, boolean isKnown)
    {
        super();
        this.id = id;
        this.name = name;
        this.passwd = passwd;
        this.hasPasswd = hasPasswd;
        this.isAlive = isAlive;
        this.isKnown = isKnown;
        this.creator = creator;
        this.userCount = userCount;
        this.maxUsers = maxUser;
    }

    public Lobby(String id, String name, String creator, int userCount, int maxUser, boolean isAlive, boolean isKnown)
    {
        super();
        this.id = id;
        this.name = name;
        this.passwd = "";
        this.hasPasswd = false;
        this.isAlive = isAlive;
        this.isKnown = isKnown;
        this.creator = creator;
        this.userCount = userCount;
        this.maxUsers = maxUser;
    }



    // Setter
    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHasPasswd(boolean hasPasswd) {
        this.hasPasswd = hasPasswd;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public void setKnown(boolean known) {
        isKnown = known;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public void setMaxUsers(int maxUsers) {
        this.maxUsers = maxUsers;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setWorldWide(boolean worldWide) {
        isWorldWide = worldWide;
    }


    // Getter
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    boolean isHasPasswd() {
        return hasPasswd;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public boolean isKnown() {
        return isKnown;
    }

    public String getCreator() {
        return this.creator;
    }

    public int getUserCount() {
        return userCount;
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    String getCurrentUserOnMax()
    {
        return getUserCount()+"/"+getMaxUsers();
    }

    public boolean isWorldWide() {
        return isWorldWide;
    }

    public DocumentReference getUid() {
        return uid;
    }

    public void setUid(DocumentReference uid) {
        this.uid = uid;
    }

    /**
     * Checks if lobby password is correct
     * @param passwd The lobby's password
     * @return password is valid
     */
    public boolean checkPassword(String passwd) {
        // TODO: request on BD with password for test, return true if equals
        return true;
    }

    @Override
    public String toString() {
        return "Lobby{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", hasPasswd=" + hasPasswd +
                ", isAlive=" + isAlive +
                ", isKnown=" + isKnown +
                ", creator=" + creator +
                ", userCount=" + userCount +
                ", maxUsers=" + maxUsers +
                '}';
    }
}
