package com.megasof.pictotala.models;

import java.util.Date;

public final class TextMessage extends Message {
    private String text;


    public TextMessage() {
        super();
    }

    public TextMessage(String authorId, String text) {
        super(authorId);
        this.text = text;
    }

    public TextMessage(String authorId, String messageId, Date dateCreated, String text) {
        super(authorId, messageId, dateCreated);
        this.text = text;
    }


    // Getter
    public String getText() {
        return text;
    }


    // Setter
    public void setText(String text) {
        this.text = text;
    }
}
