package com.megasof.pictotala.models;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;

import com.google.firebase.auth.FirebaseAuth;
import com.megasof.pictotala.R;

import java.util.List;
import java.util.Objects;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private Context mContext;
    private List<Message> mData;
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();


    public ChatAdapter(Context mContext, List<Message> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.chat_item, parent, false);

        return new ChatAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatAdapter.MyViewHolder holder, int i) {
        if (mData.get(i).getClass() == TextMessage.class) {
            TextMessage textMessage = (TextMessage) mData.get(i);
            holder.text.setText(textMessage.getText());
            holder.image.setVisibility(View.INVISIBLE);
        } else {
            PictureMessage pictMess = (PictureMessage) mData.get(i);
            holder.text.setVisibility(View.INVISIBLE);
            holder.image.setImageURI(Uri.parse(pictMess.getPicturePath()));
        }

        Message message = mData.get(i);

        holder.date.setText(DateFormat.getDateTimeInstance().format(message.getDateCreated()));
        holder.sid.setText(message.getMessageId());

        if (Objects.equals(message.getAuthorId(), mAuth.getUid())) {
            ViewGroup.MarginLayoutParams marginLayout = (ViewGroup.MarginLayoutParams) holder.card.getLayoutParams();
            marginLayout.leftMargin = 160;
            marginLayout.rightMargin = 8;
            holder.card.setLayoutParams(marginLayout);
            holder.authorName.setText(mContext.getString(R.string.cardChat_me));
        } else {
            ViewGroup.MarginLayoutParams marginLayout = (ViewGroup.MarginLayoutParams) holder.card.getLayoutParams();
            marginLayout.rightMargin = 160;
            marginLayout.leftMargin = 8;
            holder.card.setLayoutParams(marginLayout);
            holder.authorName.setText(message.getAuthorName());
        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        TextView text, date, sid, authorName;
        ImageView image;
        View divider;
        ConstraintLayout layout;

        MyViewHolder(View itemView) {
            super(itemView);
            this.card = itemView.findViewById(R.id.cv_message);
            this.date = itemView.findViewById(R.id.tv_date);
            this.text = itemView.findViewById(R.id.tv_message);
            this.image = itemView.findViewById(R.id.iv_image);
            this.sid = itemView.findViewById(R.id.tv_sidMessage);
            this.authorName = itemView.findViewById(R.id.tv_nameAuthor);
            this.divider = itemView.findViewById(R.id.divider_chatItem);
            this.layout = itemView.findViewById(R.id.cv_cardChat);
        }
    }
}