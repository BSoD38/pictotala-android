package com.megasof.pictotala.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.megasof.pictotala.Globals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import javax.annotation.Nullable;

import static android.support.constraint.Constraints.TAG;

public final class DBManager {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    /**
     * Listens for lobby list updates in real-time, and updates the lobby array in Globals whenever there's a change in Firestore.
     * @param lobbyAdapter The lobby list adapter adapter, used to force a list update
     * @return The lobby listener, is stored in order to stop listening for updates whenever needed
     */
    public ListenerRegistration startListeningLobbyUpdates(final LobbyAdapter lobbyAdapter) {
        final CollectionReference colRef = db.collection("lobbies");
        return colRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                Globals.getLobbyList().clear();
                if(queryDocumentSnapshots != null) {
                    for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                        Lobby result = document.toObject(Lobby.class);
                        assert result != null;
                        result.setId(document.getId());
                        Globals.getLobbyList().add(result);
                    }
                }
                lobbyAdapter.notifyDataSetChanged();
            }
        });
    }


    /**
     * Refreshes the lobby list in the Globals Lobby array once. Doesn't require a SwipeRefreshLayout.
     * @param lobbyAdapter The lobby list adapter adapter, used to force a list update
     */
    public void getLobbies(final LobbyAdapter lobbyAdapter) {
        db.collection("lobbies")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Globals.getLobbyList().clear();
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Lobby result = document.toObject(Lobby.class);
                                result.setId(document.getId());
                                Globals.getLobbyList().add(result);
                            }
                            lobbyAdapter.notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }


    /**
     * Refreshes the lobby list in the Globals Lobby array once.
     * @param lobbyAdapter The activity's or fragment's adapter that contains the lobby list, used to force a list update
     * @param swipeRefreshLayout The SwipeRefreshLayout used in the current activity. Used to hide the layout when finished.
     */
    public void getLobbies(final LobbyAdapter lobbyAdapter, final SwipeRefreshLayout swipeRefreshLayout) {
        db.collection("lobbies")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Globals.getLobbyList().clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Lobby result = document.toObject(Lobby.class);
                                result.setId(document.getId());
                                Globals.getLobbyList().add(result);
                            }
                            lobbyAdapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    /**
     * Creates a lobby on the database.
     * @param name The lobby's name
     * @param password The lobby's password, if any
     * @param context The app's context, used to move into the chat activity of the newly created lobby
     */
    public void createLobby(final String name, final String password, final Context context) {
        if(mAuth.getUid() != null) {
            db.collection("users").document(mAuth.getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    HashMap<String, Object> data = new HashMap<>();
                    data.put("name", name);
                    data.put("uid", db.collection("users").document(mAuth.getUid()));
                    data.put("creator", documentSnapshot.get("name"));

                    if (password != null && !password.isEmpty()) {
                        data.put("hasPasswd", true);
                        data.put("password", password);
                    } else {
                        data.put("hasPasswd", false);
                        data.put("password", null);
                    }
                    db.collection("lobbies").add(data)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Globals.enterLobby(context, documentReference.getId());
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(context, "Erreur : " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                }
            });
        }
    }

    /**
     * Listens for messages in a lobby, and updates in real-time.
     * @param sid The lobby's SID
     * @param messages The message array to update
     * @param chatAdapter The message list adapter. Used to force a list update when a new message is received.
     * @return The message listener, stored in order to stop listening for updates.
     */
    public ListenerRegistration listenForMessages(String sid, final ArrayList<Message> messages, final ChatAdapter chatAdapter) {
        return db.collection("lobbies")
                .document(sid)
                .collection("messages")
                .orderBy("dateCreated", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        messages.clear();
                        assert queryDocumentSnapshots != null;
                        for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                            TextMessage message = document.toObject(TextMessage.class);
                            messages.add(message);
                        }
                        chatAdapter.notifyDataSetChanged();
                    }
                });
    }

    /**
     * Sends a message in a lobby.
     * @param sid The lobby's SID
     * @param message The message to send
     */
    public void sendMessage(final String sid, final Message message) {
        if(mAuth.getUid() != null) {
            db.collection("users").document(mAuth.getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    Message m = message;
                    m.setAuthorName((String) documentSnapshot.get("name"));
                    db.collection("lobbies").document(sid).collection("messages").add(m);
                }
            });
        }
    }
}
