package com.megasof.pictotala;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.megasof.pictotala.activities.ChatActivity;
import com.megasof.pictotala.models.Lobby;

import java.util.ArrayList;
import java.util.List;

public final class Globals extends Application {
    private static List<Lobby> gLobbyList = new ArrayList<>();
    public static final String INTENT_LOBBY_SID = "sid";

    /**
     * Connects to a lobby. TODO: Handle passwords
     * @param context The app's context
     * @param sid The lobby's SID
     */
    public static void enterLobby(Context context, String sid) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(INTENT_LOBBY_SID, sid);
        context.startActivity(intent);
    }


    public static List<Lobby> getLobbyList() {
        return gLobbyList;
    }
}