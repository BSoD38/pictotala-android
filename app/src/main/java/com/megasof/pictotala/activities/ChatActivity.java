package com.megasof.pictotala.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.ListenerRegistration;
import com.megasof.pictotala.Globals;
import com.megasof.pictotala.R;
import com.megasof.pictotala.models.ChatAdapter;
import com.megasof.pictotala.models.DBManager;
import com.megasof.pictotala.models.Lobby;
import com.megasof.pictotala.models.Message;
import com.megasof.pictotala.models.TextMessage;
import com.megasof.pictotala.models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChatActivity extends AppCompatActivity {

    public static List<User> participants;
    static ArrayList<Message> messages = new ArrayList<>();
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private final DBManager dbManager = new DBManager();
    private String sid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        sid = getIntent().getStringExtra(Globals.INTENT_LOBBY_SID);
        final ChatAdapter adapter = new ChatAdapter(this, messages);

        setTitle(Globals.getLobbyList().get(findLobby(sid)).getName());

        ListenerRegistration messageListener = new DBManager().listenForMessages(getIntent().getStringExtra(Globals.INTENT_LOBBY_SID), messages, adapter);

        participants = new ArrayList<>();

        RecyclerView recyclerView = findViewById(R.id.rv_messageList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Button send = findViewById(R.id.b_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText message = findViewById(R.id.et_message);
                String text = message.getText().toString();

                if (!text.equals("")) {
                    dbManager.sendMessage(sid, new TextMessage(mAuth.getUid(),null, new Date(), text));
                    message.setText("");
                }
            }
        });
    }

    /**
     * Returns a lobby's ID in the lobby list by its SID.
     * @param sid The lobby's SID
     * @return the lobby's position in the lobby array in the Globals class
     */
    private int findLobby(String sid) {
        List<Lobby> lobbyList = Globals.getLobbyList();

        for (Lobby lob:lobbyList) {
            if (sid.equals(lob.getId())) {
                return lobbyList.indexOf(lob);
            }
        }

        return -1;
    }

}
