package com.megasof.pictotala.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.megasof.pictotala.fragments.CreationLobbyDialog;
import com.megasof.pictotala.Globals;
import com.megasof.pictotala.R;
import com.megasof.pictotala.fragments.WorldwideFragment;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public class MainMenu extends AppCompatActivity implements WorldwideFragment.OnFragmentInteractionListener {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationDrawer;
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private WorldwideFragment worldwide = new WorldwideFragment();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        getFragmentManager().beginTransaction().replace(R.id.appContent, worldwide).commit();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new CreationLobbyDialog();
                newFragment.show(getSupportFragmentManager(), "Lobby");
            }
        });

        mDrawerLayout = findViewById(R.id.drawer_layout);

        navigationDrawer = findViewById(R.id.nav_view);
        navigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        // set item as selected to persist highlight
                        switch (menuItem.getItemId()) {
                            case R.id.nav_login:
                                List<AuthUI.IdpConfig> providers = Arrays.asList(
                                        new AuthUI.IdpConfig.EmailBuilder().build(),
                                        new AuthUI.IdpConfig.GoogleBuilder().build());

                                startActivityForResult(
                                        AuthUI.getInstance()
                                                .createSignInIntentBuilder()
                                                .setAvailableProviders(providers)
                                                .build(),
                                        123);
                                return true;
                            case R.id.nav_logout:
                                AuthUI.getInstance()
                                        .signOut(MainMenu.this)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            public void onComplete(@NonNull Task<Void> task) {
                                                updateUserState(mAuth.getCurrentUser());
                                                Toast.makeText(MainMenu.this, MainMenu.this.getString(R.string.toast_info_disconnected), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                return true;
                        }
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUserState(currentUser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 123) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Toast.makeText(MainMenu.this, this.getString(R.string.toast_info_connected), Toast.LENGTH_SHORT).show();
                updateUserState(user);
            } else {
                if (response != null) {
                    Toast.makeText(MainMenu.this, this.getString(R.string.toast_err_connect_failed) + Objects.requireNonNull(response.getError()).getErrorCode(), Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onSaveInstanceState(Bundle outInstanceState) {
        super.onSaveInstanceState(outInstanceState);
        outInstanceState.putInt("value", 1);
    }

    /**
     * Updates the main activity depending on whether the user is logged in or not
     * @param user The current user
     */
    private void updateUserState(FirebaseUser user) {
        Menu menu = navigationDrawer.getMenu();
        if (user == null) {
            menu.findItem(R.id.nav_login).setVisible(true);
            menu.findItem(R.id.nav_logout).setVisible(false);
            worldwide.stopListening();
            Globals.getLobbyList().clear();
        } else {
            menu.findItem(R.id.nav_login).setVisible(false);
            menu.findItem(R.id.nav_logout).setVisible(true);
            worldwide.startListening();
        }
    }
}

//Snackbar.make(view, "Ajout !", Snackbar.LENGTH_LONG)
//        .setAction("Action", null).show();