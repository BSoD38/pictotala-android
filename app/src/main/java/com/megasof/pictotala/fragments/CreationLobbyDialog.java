package com.megasof.pictotala.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.megasof.pictotala.R;
import com.megasof.pictotala.models.DBManager;

public class CreationLobbyDialog extends DialogFragment {

    View viewParent;
    private DBManager dbManager = new DBManager();
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        viewParent = inflater.inflate(R.layout.dialog_creation_lobby, null);

        final EditText et_lobbyPasswd = viewParent.findViewById(R.id.et_lobbyPasswd);
        CheckBox hasPasswd = viewParent.findViewById(R.id.cb_withPasswd);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(viewParent)
                // Add action buttons
                .setPositiveButton(R.string.createDialog_validateButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final EditText lobbyName = viewParent.findViewById(R.id.et_lobbyName);
                        EditText lobbyPasswd = viewParent.findViewById(R.id.et_lobbyPasswd);
                        dbManager.createLobby(lobbyName.getText().toString(), lobbyPasswd.getText().toString(), getContext());
                    }
                });
        et_lobbyPasswd.setVisibility(View.GONE);
        hasPasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    et_lobbyPasswd.setVisibility(View.VISIBLE);
                } else {
                    et_lobbyPasswd.setVisibility(View.GONE);
                }
            }
        });
        return builder.create();
    }
}
