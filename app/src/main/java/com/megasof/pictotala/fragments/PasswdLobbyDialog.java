package com.megasof.pictotala.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.megasof.pictotala.Globals;
import com.megasof.pictotala.R;
import com.megasof.pictotala.models.Lobby;

import java.util.List;


public class PasswdLobbyDialog extends DialogFragment {
    View viewParent;
    String idLobby;
    String name;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        viewParent = inflater.inflate(R.layout.dialog_lobby_passwd, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            idLobby = bundle.getString("id", "");
            name = bundle.getString("name", "");
        }

        builder.setView(viewParent)
                .setPositiveButton(R.string.passwdDialog_validateButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText lobbyPasswd = viewParent.findViewById(R.id.et_testLobbyPasswd);
                        List<Lobby> lobbyList = Globals.getLobbyList();
                        boolean isGudPasswd = false;

                        for (Lobby lob : lobbyList) {
                            if (lob.getId().equals(idLobby) && lob.checkPassword(lobbyPasswd.getText().toString()))
                                isGudPasswd = true;
                        }

                        if (isGudPasswd) {
                            Globals.enterLobby(getContext(),idLobby);
                        } else {
                            Toast.makeText(getContext(), R.string.passwdDialog_wrongPasswdToast, Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.createDialog_cancelButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getContext(), "Annulé", Toast.LENGTH_SHORT).show();
                    }
                });

        return builder.create();
    }
}



