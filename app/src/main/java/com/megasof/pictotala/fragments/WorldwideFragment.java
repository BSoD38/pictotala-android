package com.megasof.pictotala.fragments;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.ListenerRegistration;
import com.megasof.pictotala.Globals;
import com.megasof.pictotala.R;
import com.megasof.pictotala.models.DBManager;
import com.megasof.pictotala.models.LobbyAdapter;

import java.util.Objects;

import static android.support.constraint.Constraints.TAG;

public class WorldwideFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private DBManager dbManager = new DBManager();
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private LobbyAdapter lobbyAdapter;
    private ListenerRegistration listener;

    public WorldwideFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            return inflater.inflate(R.layout.content_main_menu, container, false);
        } catch (Exception e) {
            Log.e(TAG, "onCreateView", e);
            throw e;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(savedInstanceState == null) {
            RecyclerView recyclerView = Objects.requireNonNull(getView()).findViewById(R.id.rv_lobbyList);
            lobbyAdapter = new LobbyAdapter(getActivity(), Globals.getLobbyList());
            recyclerView.setAdapter(lobbyAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            final SwipeRefreshLayout swipeRefresh = getView().findViewById(R.id.swiperefresh);
            if(mAuth.getCurrentUser() != null) {
                swipeRefresh.setOnRefreshListener(
                        new SwipeRefreshLayout.OnRefreshListener() {
                            @Override
                            public void onRefresh() {
                                // This method performs the actual data-refresh operation.
                                // The method calls setRefreshing(false) when it's finished.
                                dbManager.getLobbies(lobbyAdapter, swipeRefresh);
                            }
                        }
                );
            } else {
                Toast.makeText(view.getContext(), view.getContext().getString(R.string.toast_info_worldwide_disconnected), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outInstanceState) {
        outInstanceState.putInt("value", 1);
    }

    /**
     * Start listening for lobby list updates.
     */
    public void startListening() {
        listener = dbManager.startListeningLobbyUpdates(lobbyAdapter);
    }

    /**
     * Stop listening for updates, if there's a listener
     */
    public void stopListening() {
        if(listener != null) {
            listener.remove();
        }
    }
}
